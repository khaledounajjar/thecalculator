﻿using System.Diagnostics;
using Xamarin.Forms;

namespace TheCalculator
{
    public partial class TheCalculatorPage : ContentPage
    {
        private Button  myButton;
        private string  tmp;
        private string  numberFirst;
        private string  numberSecond;
        private string  sign;
        private float   result;


        public TheCalculatorPage()
        {
            InitializeComponent();
            tmp = "";
            numberFirst = "";
            numberSecond = "";
            sign = "";
            result = 0;
        }

        void numberClicked(object sender, System.EventArgs e)
        {
            myButton = (Button)sender;

            Debug.WriteLine(numberFirst);
            Debug.WriteLine(sign);
            Debug.WriteLine(numberSecond);

            if (sign == "" && (myButton.Text == "/" || myButton.Text == "X" || myButton.Text == "-" || myButton.Text == "+"))
            {
                numberFirst = tmp;
                sign = myButton.Text;
                tmp = "";
                return;
            }
            if (myButton.Text != "/" && myButton.Text != "X" && myButton.Text != "-" && myButton.Text != "+")
            {
                tmp = tmp + myButton.Text;
                screenLabel.Text = $"{tmp}";
            }
            else
                sign = myButton.Text;
        }

        void equalClicked(object sender, System.EventArgs e)
        {
            if (numberFirst == "" || sign == "")
                return;
            numberSecond = tmp;

            if (sign == "/")
            {
                if (int.Parse(numberSecond) != 0)
                    result = float.Parse(numberFirst) / float.Parse(numberSecond);
                else
                {
                    screenLabel.Text = "Error";
                    numberFirst = "";
                    tmp = "0";
                    numberSecond = "";
                    sign = "";
                    return;
                }
            }
            else if (sign == "X")
                result = float.Parse(numberFirst) * float.Parse(numberSecond);
            else if (sign == "-")
                result = float.Parse(numberFirst) - float.Parse(numberSecond);
            else if (sign == "+")
                result = float.Parse(numberFirst) + float.Parse(numberSecond);

           
            screenLabel.Text = $"{result.ToString()}";
            numberFirst = result.ToString();
            tmp = numberFirst;
            numberSecond = "";
            sign = "";
        }

        void clearClicked(object sender, System.EventArgs e)
        {
            tmp = "";
            numberFirst = "";
            numberSecond = "";
            sign = "";
            screenLabel.Text = $"0";
        }
    }
}
